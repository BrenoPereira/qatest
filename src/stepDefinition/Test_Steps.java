package stepDefinition;

import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Test_Steps {
	public static WebDriver driver;
	@Given("^User is on Home Page$")
	public void user_is_on_Home_Page() throws Throwable {
        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("http://qa-test.avenuecode.com/");
	}
	
	@Given("^User is on Tasks Page$")
	public void user_is_on_Task_Add_Page() throws Throwable {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("http://qa-test.avenuecode.com/tasks");
	}

	@When("^User Navigate to LogIn Page$")
	public void user_Navigate_to_LogIn_Page() throws Throwable {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElement(By.linkText("Sign In")).click();
	}

	@When("^User enters UserName and Password$")
	public void user_enters_UserName_and_Password() throws Throwable {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElement(By.id("user_email")).sendKeys("breno.alpereira@gmail.com"); 	 
	    driver.findElement(By.id("user_password")).sendKeys("brenopereira");
	    driver.findElement(By.name("commit")).click();
	}

	@When("^User Clicks on My Tasks$")
	public void user_Clicks_My_Tasks_Button() throws Throwable {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElement(By.className("btn btn-lg btn-success")).click();
	}
	
	@When("^User will see the message for ToDo List of the day$")
	public void user_see_message_ToDoList_Today() throws Throwable {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		WebElement element = driver.findElement(By.xpath("//span[text()='Breno de Almeida Pereira's ToDo List']"));
		String strng = element.getText();
		System.out.println(strng);
		Assert.assertEquals("Breno de Almeida Pereira's ToDo List", strng);
	}
	
	@When("^User Clicks on Add Task$")
	public void user_Clicks_Add_Task_Button() throws Throwable {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElement(By.className("input-group-addon glyphicon glyphicon-plus")).click();
	}
	
	@When("^User enters Task Name and Add Task$")
	public void user_Add_Task_Name() throws Throwable {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElement(By.className("ng-scope ng-binding editable editable-click editable-empty")).click(); 	 
		driver.findElement(By.className("editable-has-buttons editable-input form-control ng-pristine ng-valid ng-touched")).sendKeys("Testing task adding"); 	 
		driver.findElement(By.className("glyphicon glyphicon-ok")).click();
	}
	
	@When("^User Clicks on My Tasks to See It Tasks Added$")
	public void user_Can_See_Task_On_List() throws Throwable {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElement(By.linkText("My Tasks")).click();
		WebElement element = driver.findElement(By.linkText("Testing task adding"));
		String strng = element.getText();
		System.out.println(strng);
		Assert.assertEquals("Testing task adding", strng);
	}
	
	@When("^User enters Task Name and Add Task by hitting enter$")
	public void user_Add_Task_Name_Hitting_Enter() throws Throwable {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElement(By.className("ng-scope ng-binding editable editable-click editable-empty")).click(); 	 
		driver.findElement(By.className("editable-has-buttons editable-input form-control ng-pristine ng-valid ng-touched")).sendKeys("Testing task adding"); 	 
		driver.findElement(By.className("btn btn-primary")).sendKeys("\n");	
	}
	
	@When("^User enters Task Name and Add Task but cancel$")
	public void user_Try_Add_Task_Name_Cancel() throws Throwable {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElement(By.className("ng-scope ng-binding editable editable-click editable-empty")).click(); 	 
		driver.findElement(By.className("editable-has-buttons editable-input form-control ng-pristine ng-valid ng-touched")).sendKeys("Testing task adding"); 	 		
		driver.findElement(By.className("btn btn-primary")).click();
		driver.findElement(By.className("glyphicon glyphicon-remove")).click();	
	}
	
	@When("^User enters Task Name and Add Task Less than three Characters$")
	public void user_Add_Task_Less_3_Characters() throws Throwable {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElement(By.className("ng-scope ng-binding editable editable-click editable-empty")).click(); 	 
		driver.findElement(By.className("editable-has-buttons editable-input form-control ng-pristine ng-valid ng-touched")).sendKeys("T"); 	 
		driver.findElement(By.className("glyphicon glyphicon-ok")).click();
	}
	
	@When("^User enters Task Name and Add Task More than max Characters$")
	public void user_Add_Task_More_250_Characters() throws Throwable {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElement(By.className("ng-scope ng-binding editable editable-click editable-empty")).click(); 	 
		driver.findElement(By.className("editable-has-buttons editable-input form-control ng-pristine ng-valid ng-touched")).sendKeys("testing adding more than 250 characters on the task name, if some error occured it's safe, but if it pass, so we will need to fix it, and testing all over again, because this is a important requisite to the system to work well and client gives them acceptance"); 	 
		driver.findElement(By.className("glyphicon glyphicon-ok")).click();
	}
	//new code init
	@When("^User Clicks on SubTasks$")
	public void user_Clicks_SubTasks_Button() throws Throwable {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElement(By.partialLinkText("Manage subtasks")).click();
	}
	
	@When("^User adds a Subtask")
	public void user_fill_Todo_Subtask() throws Throwable {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElement(By.id("new_sub_task")).sendKeys("Testing subtask"); 
		driver.findElement(By.id("dueDate")).sendKeys("05/11/2016");
		driver.findElement(By.id("add-subtask")).click();
	}
	
	@When("^User enters Todo on Subtask more than max Characters$")
	public void user_fill_Todo_Subtask_Max_Char() throws Throwable {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElement(By.id("edit_task")).sendKeys("testing adding more than 250 characters on the task name, if some error occured it's safe, but if it pass, so we will need to fix it, and testing all over again, because this is a important requisite to the system to work well and client gives them acceptance"); 	 
		driver.findElement(By.id("dueDate")).sendKeys("05/11/2016");
		driver.findElement(By.id("add-subtask")).click();
	}
	
	@When("^User enters Todo on Subtask wrong Date$")
	public void user_fill_Todo_Subtask_Wrong_Date() throws Throwable {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElement(By.id("edit_task")).sendKeys("testing adding more than 250 characters on the task name, if some error occured it's safe, but if it pass, so we will need to fix it, and testing all over again, because this is a important requisite to the system to work well and client gives them acceptance"); 	 
		driver.findElement(By.id("dueDate")).sendKeys("05-11-2016");
		driver.findElement(By.id("add-subtask")).click();
	}
	
	//new code fim
	@When("^Message displayed Login Successfully$")
	public void message_displayed_Login_Successfully() throws Throwable {
		System.out.println("Login Successfully");
	}

	@When("^User LogOut from the Application$")
	public void user_LogOut_from_the_Application() throws Throwable {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElement(By.linkText("Sign out")).click();
	}

	@Then("^Message displayed Logout Successfully$")
	public void message_displayed_Logout_Successfully() throws Throwable {
        System.out.println("LogOut Successfully");
	}
	
	@Then("^Message displayed Add Successfully$")
	public void message_displayed_Add_Successfully() throws Throwable {
        System.out.println("Add Successfully");
	}

}