Feature: Adding Action

Scenario: Successful Login with Valid Credentials
	Given User is on Home Page
	When User Navigate to LogIn Page
	And User enters UserName and Password
	Then Message displayed Login Successfully
	
Scenario: Successful Add a Task
	When User Clicks on My Tasks
	And User will see the message for ToDo List of the day
	And User Clicks on Add Task
	And User enters Task Name and Add Task
	Then Message displayed Add Successfully

Scenario: Successful LogOut
	When User LogOut from the Application
	Then Message displayed Logout Successfully