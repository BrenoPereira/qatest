Feature: Adding Cancel Action

Scenario: Successful Login with Valid Credentials
	Given User is on Home Page
	When User Navigate to LogIn Page
	And User enters UserName and Password
	Then Message displayed Login Successfully
	
Scenario: Successful Add a Task
	When User Clicks on My Tasks to See It Tasks Added
	Then Message displayed Add Successfully

Scenario: Successful LogOut
	When User LogOut from the Application
	Then Message displayed Logout Successfully